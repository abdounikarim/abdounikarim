<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class SendMail
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var Template
     */
    private $template;

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $template, EntityManagerInterface $entityManager)
    {
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
        $this->template = $template;
    }

    public function send($data)
    {
        $message = (new \Swift_Message('Nouveau message depuis abdounikarim.com'))
            ->setFrom($data->getEmail())
            ->setTo('abdounikarim@gmail.com')
            ->setBody(
                $this->template->render(
                    'mail/template_mail.html.twig',
                    ['data' => $data ]),
                'text/html'
            )
        ;
        $this->mailer->send($message);
    }

    public function save($data)
    {
        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }
}