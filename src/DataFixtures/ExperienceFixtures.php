<?php

namespace App\DataFixtures;

use App\Entity\Experience;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ExperienceFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $oc = new Experience();
        $oc->setTitle('Mentor des métiers du web');
        $oc->setPeriod('Juin 2015 à aujourd\'hui');
        $oc->setImage('oc.png');
        $oc->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac ipsum at quam lobortis hendrerit sit amet sit amet felis. Nullam rhoncus consectetur magna nec vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam, ante quis accumsan vehicula, lectus ligula vestibulum purus, in suscipit neque augue vel dolor. Proin lacinia luctus est, a fringilla mi dictum sit amet. Nam aliquet urna quis ornare pharetra. Nunc hendrerit convallis bibendum.');
        $manager->persist($oc);

        $iesa = new Experience();
        $iesa->setTitle('Intervenant web');
        $iesa->setPeriod('Septembre à Décembre 2015');
        $iesa->setImage('iesa.png');
        $iesa->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac ipsum at quam lobortis hendrerit sit amet sit amet felis. Nullam rhoncus consectetur magna nec vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam, ante quis accumsan vehicula, lectus ligula vestibulum purus, in suscipit neque augue vel dolor. Proin lacinia luctus est, a fringilla mi dictum sit amet. Nam aliquet urna quis ornare pharetra. Nunc hendrerit convallis bibendum.');
        $manager->persist($iesa);

        $icademie = new Experience();
        $icademie->setTitle('Rédacteur de sujets d\'examen');
        $icademie->setPeriod('Mars 2018 à aujourd\'hui');
        $icademie->setImage('icademie.png');
        $icademie->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac ipsum at quam lobortis hendrerit sit amet sit amet felis. Nullam rhoncus consectetur magna nec vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam, ante quis accumsan vehicula, lectus ligula vestibulum purus, in suscipit neque augue vel dolor. Proin lacinia luctus est, a fringilla mi dictum sit amet. Nam aliquet urna quis ornare pharetra. Nunc hendrerit convallis bibendum.');
        $manager->persist($icademie);

        $ak = new Experience();
        $ak->setTitle('Développeur');
        $ak->setPeriod('Février 2015 à aujourd\'hui');
        $ak->setImage('ak.png');
        $ak->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac ipsum at quam lobortis hendrerit sit amet sit amet felis. Nullam rhoncus consectetur magna nec vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam, ante quis accumsan vehicula, lectus ligula vestibulum purus, in suscipit neque augue vel dolor. Proin lacinia luctus est, a fringilla mi dictum sit amet. Nam aliquet urna quis ornare pharetra. Nunc hendrerit convallis bibendum.');
        $manager->persist($ak);

        $manager->flush();
    }
}
