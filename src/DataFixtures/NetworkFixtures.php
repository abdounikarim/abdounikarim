<?php

namespace App\DataFixtures;

use App\Entity\Network;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class NetworkFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $aurore = new Network();
        $aurore->setLastname('Gaucher');
        $aurore->setFirstname('Aurore');
        $aurore->setTitle('Chef de projet / Développeuse web');
        $aurore->setImage('aurore.png');
        $aurore->setEmail('contact@auroregaucher.com');
        $aurore->setSite('http://www.auroregaucher.com');
        $aurore->setLinkedin('https://www.linkedin.com/in/aurore-gaucher/');
        $aurore->setGithub('https://github.com/Aigie52');
        $aurore->setStar(5);
        $aurore->setDescription('Membre de l\'équipe');
        $aurore->setTeam(1);

        $manager->persist($aurore);

        $guillaume = new Network();
        $guillaume->setLastname('Loulier');
        $guillaume->setFirstname('Guillaume');
        $guillaume->setTitle('Développeur PHP/Symfony');
        $guillaume->setImage('guillaume.jpg');
        $guillaume->setEmail('guillaume.loulier@hotmail.fr');
        $guillaume->setSite('http://www.guillaumeloulier.fr/fr/ ');
        $guillaume->setLinkedin('https://www.linkedin.com/in/guillaume-loulier-594438101/');
        $guillaume->setGithub('https://github.com/Guikingone');
        $guillaume->setStar(5);
        $guillaume->setDescription('Membre de l\'équipe');
        $guillaume->setTeam(1);

        $manager->persist($guillaume);

        $jordane = new Network();
        $jordane->setLastname('Lacroix');
        $jordane->setFirstname('Jordane');
        $jordane->setTitle('Frontend / Designer');
        $jordane->setImage('jordane.png');
        $jordane->setLinkedin('https://www.linkedin.com/in/jordanelacroix/');
        $jordane->setStar(5);
        $jordane->setDescription('Membre de l\'équipe');
        $jordane->setTeam(1);

        $manager->persist($jordane);

        $severine = new Network();
        $severine->setLastname('Carré');
        $severine->setFirstname('Séverine');
        $severine->setTitle('Mentore en intégration web et gestion de projet');
        $severine->setImage('severine.png');
        $severine->setEmail('severine.carre@orange.fr');
        $severine->setLinkedin('https://www.linkedin.com/in/carreseverine/');
        $severine->setStar(5);
        $severine->setDescription('Membre de l\'équipe');
        $severine->setTeam(1);

        $manager->persist($severine);

        $jean = new Network();
        $jean->setLastname('Dupont');
        $jean->setFirstname('Jean');
        $jean->setTitle('Designer');
        $jean->setImage('homme.png');
        $jean->setEmail('jean@dupont.com');
        $jean->setSite('https://jeandupont.com');
        $jean->setLinkedin('https://www.linkedin.com/');
        $jean->setGithub('https://www.github.com/');
        $jean->setFacebook('https://www.facebook.com/');
        $jean->setStar(4);
        $jean->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend tincidunt vulputate. Duis fermentum nibh nibh, non lacinia turpis molestie in. Sed tincidunt sem tincidunt, interdum libero vitae, ultrices nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus in eleifend risus. Nunc tristique egestas sollicitudin. Quisque hendrerit lobortis quam, non iaculis urna pellentesque nec.');
        $jean->setTeam(0);

        $manager->persist($jean);

        $jeanne = new Network();
        $jeanne->setLastname('Dupont');
        $jeanne->setFirstname('Jeanne');
        $jeanne->setTitle('Développeuse');
        $jeanne->setImage('femme.png');
        $jeanne->setEmail('jeanne@dupont.com');
        $jeanne->setSite('https://jeannedupont.com');
        $jeanne->setLinkedin('https://www.linkedin.com/');
        $jeanne->setGithub('https://www.github.com/');
        $jeanne->setFacebook('https://www.facebook.com/');
        $jeanne->setStar(5);
        $jeanne->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend tincidunt vulputate. Duis fermentum nibh nibh, non lacinia turpis molestie in. Sed tincidunt sem tincidunt, interdum libero vitae, ultrices nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus in eleifend risus. Nunc tristique egestas sollicitudin. Quisque hendrerit lobortis quam, non iaculis urna pellentesque nec.');
        $jeanne->setTeam(0);

        $manager->persist($jeanne);

        $romain = new Network();
        $romain->setLastname('Dupont');
        $romain->setFirstname('Romain');
        $romain->setTitle('Marketeur');
        $romain->setImage('homme2.png');
        $romain->setEmail('romain@dupont.com');
        $romain->setSite('https://romaindupont.com');
        $romain->setLinkedin('https://www.linkedin.com/');
        $romain->setGithub('https://www.github.com/');
        $romain->setFacebook('https://www.facebook.com/');
        $romain->setStar(0);
        $romain->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend tincidunt vulputate. Duis fermentum nibh nibh, non lacinia turpis molestie in. Sed tincidunt sem tincidunt, interdum libero vitae, ultrices nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus in eleifend risus. Nunc tristique egestas sollicitudin. Quisque hendrerit lobortis quam, non iaculis urna pellentesque nec.');
        $romain->setTeam(0);

        $manager->persist($romain);

        $rachel = new Network();
        $rachel->setLastname('Dupont');
        $rachel->setFirstname('Rachel');
        $rachel->setTitle('Cheffe de projet');
        $rachel->setImage('femme2.png');
        $rachel->setEmail('rachel@dupont.com');
        $rachel->setSite('https://racheldupont.com');
        $rachel->setLinkedin('https://www.linkedin.com/');
        $rachel->setGithub('https://www.github.com/');
        $rachel->setFacebook('https://www.facebook.com/');
        $rachel->setStar(3);
        $rachel->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend tincidunt vulputate. Duis fermentum nibh nibh, non lacinia turpis molestie in. Sed tincidunt sem tincidunt, interdum libero vitae, ultrices nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus in eleifend risus. Nunc tristique egestas sollicitudin. Quisque hendrerit lobortis quam, non iaculis urna pellentesque nec.');
        $rachel->setTeam(0);

        $manager->persist($rachel);

        $manager->flush();
    }
}
