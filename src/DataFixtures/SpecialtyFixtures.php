<?php

namespace App\DataFixtures;

use App\Entity\Specialty;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SpecialtyFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $integration = new Specialty();
        $integration->setName('Intégration');
        $manager->persist($integration);
        $this->addReference('integration', $integration);

        $frontend = new Specialty();
        $frontend->setName('Frontend');
        $manager->persist($frontend);
        $this->addReference('frontend', $frontend);

        $backend = new Specialty();
        $backend->setName('Backend');
        $manager->persist($backend);
        $this->addReference('backend', $backend);

        $manager->flush();
    }
}
