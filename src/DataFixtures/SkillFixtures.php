<?php

namespace App\DataFixtures;

use App\Entity\Skill;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SkillFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $html = new Skill();
        $html->setName('HTML');
        $html->setImage('html.svg');
        $manager->persist($html);
        $this->addReference('html', $html);

        $css = new Skill();
        $css->setName('CSS');
        $css->setImage('css.svg');
        $manager->persist($css);
        $this->addReference('css', $css);

        $bootstrap = new Skill();
        $bootstrap->setName('Bootstrap');
        $bootstrap->setImage('bootstrap.svg');
        $manager->persist($bootstrap);
        $this->addReference('bootstrap', $bootstrap);

        $javascript = new Skill();
        $javascript->setName('JavaScript');
        $javascript->setImage('javascript.svg');
        $manager->persist($javascript);
        $this->addReference('javascript', $javascript);

        $jquery = new Skill();
        $jquery->setName('jQuery');
        $jquery->setImage('jquery.svg');
        $manager->persist($jquery);
        $this->addReference('jquery', $jquery);

        $php = new Skill();
        $php->setName('PHP');
        $php->setImage('php.svg');
        $manager->persist($php);
        $this->addReference('php', $php);

        $mysql = new Skill();
        $mysql->setName('MySQL');
        $mysql->setImage('mysql.svg');
        $manager->persist($mysql);
        $this->addReference('mysql', $mysql);

        $wordpress = new Skill();
        $wordpress->setName('WordPress');
        $wordpress->setImage('wordpress.svg');
        $manager->persist($wordpress);
        $this->addReference('wordpress', $wordpress);

        $symfony = new Skill();
        $symfony->setName('Symfony');
        $symfony->setImage('symfony.svg');
        $manager->persist($symfony);
        $this->addReference('symfony', $symfony);

        $git = new Skill();
        $git->setName('Git');
        $git->setImage('git.svg');
        $manager->persist($git);
        $this->addReference('git', $git);

        $manager->flush();
    }
}
