<?php

namespace App\DataFixtures;

use App\Entity\Video;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class VideoFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $video1 = new Video();
        $video1->setName('Who\'s MoOC ? Philippe Beck, Premium Plus !');
        $video1->setUrl('https://www.youtube.com/embed/L3Hg0XxtJMM');
        $manager->persist($video1);

        $video2 = new Video();
        $video2->setName('Who\'s MoOC? Nos mentors !');
        $video2->setUrl('https://www.youtube.com/embed/Fuy5i01iJNs');
        $manager->persist($video2);

        $video3 = new Video();
        $video3->setName('Who\'s MoOC? Partners : IESA Multimédia');
        $video3->setUrl('https://www.youtube.com/embed/A9Xu0-JMvuM');
        $manager->persist($video3);

        $video4 = new Video();
        $video4->setName('Comment la technologie reconnecte les individus');
        $video4->setUrl('https://www.youtube.com/embed/BS_9XIh0Jqc');
        $manager->persist($video4);

        $manager->flush();
    }
}
