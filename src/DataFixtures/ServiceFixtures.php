<?php

namespace App\DataFixtures;

use App\Entity\Service;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ServiceFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $dev = new Service();
        $dev->setName('Développement de projet web');
        $dev->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac ipsum at quam lobortis hendrerit sit amet sit amet felis. Nullam rhoncus consectetur magna nec vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam, ante quis accumsan vehicula, lectus ligula vestibulum purus, in suscipit neque augue vel dolor. Proin lacinia luctus est, a fringilla mi dictum sit amet. Nam aliquet urna quis ornare pharetra. Nunc hendrerit convallis bibendum.');
        $dev->setImage('dev.jpg');
        $dev->setIcon('<ion-icon name="ios-code" class="ios-lock"></ion-icon>');
        $manager->persist($dev);

        $formation = new Service();
        $formation->setName('Formation et conseil');
        $formation->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac ipsum at quam lobortis hendrerit sit amet sit amet felis. Nullam rhoncus consectetur magna nec vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam, ante quis accumsan vehicula, lectus ligula vestibulum purus, in suscipit neque augue vel dolor. Proin lacinia luctus est, a fringilla mi dictum sit amet. Nam aliquet urna quis ornare pharetra. Nunc hendrerit convallis bibendum.');
        $formation->setImage('formation.png');
        $formation->setIcon('<ion-icon name="ios-information-circle" class="ios-lock"></ion-icon>');
        $manager->persist($formation);

        $course = new Service();
        $course->setName('Création de cours');
        $course->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac ipsum at quam lobortis hendrerit sit amet sit amet felis. Nullam rhoncus consectetur magna nec vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam, ante quis accumsan vehicula, lectus ligula vestibulum purus, in suscipit neque augue vel dolor. Proin lacinia luctus est, a fringilla mi dictum sit amet. Nam aliquet urna quis ornare pharetra. Nunc hendrerit convallis bibendum.');
        $course->setImage('course.jpg');
        $course->setIcon('<ion-icon name="ios-list" class="ios-lock"></ion-icon>');
        $manager->persist($course);

        $job = new Service();
        $job->setName('Accompagnement à l\'emploi');
        $job->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac ipsum at quam lobortis hendrerit sit amet sit amet felis. Nullam rhoncus consectetur magna nec vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam, ante quis accumsan vehicula, lectus ligula vestibulum purus, in suscipit neque augue vel dolor. Proin lacinia luctus est, a fringilla mi dictum sit amet. Nam aliquet urna quis ornare pharetra. Nunc hendrerit convallis bibendum.');
        $job->setImage('job.jpg');
        $job->setIcon('<ion-icon name="ios-help" class="ios-lock"></ion-icon>');
        $manager->persist($job);

        $code = new Service();
        $code->setName('Optimisation de code source');
        $code->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac ipsum at quam lobortis hendrerit sit amet sit amet felis. Nullam rhoncus consectetur magna nec vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam, ante quis accumsan vehicula, lectus ligula vestibulum purus, in suscipit neque augue vel dolor. Proin lacinia luctus est, a fringilla mi dictum sit amet. Nam aliquet urna quis ornare pharetra. Nunc hendrerit convallis bibendum.');
        $code->setImage('code.png');
        $code->setIcon('<ion-icon name="ios-code" class="ios-lock"></ion-icon>');
        $manager->persist($code);

        $site = new Service();
        $site->setName('Refonte de site');
        $site->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac ipsum at quam lobortis hendrerit sit amet sit amet felis. Nullam rhoncus consectetur magna nec vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam, ante quis accumsan vehicula, lectus ligula vestibulum purus, in suscipit neque augue vel dolor. Proin lacinia luctus est, a fringilla mi dictum sit amet. Nam aliquet urna quis ornare pharetra. Nunc hendrerit convallis bibendum.');
        $site->setImage('site.jpg');
        $site->setIcon('<ion-icon name="ios-desktop" class="ios-lock"></ion-icon>');
        $manager->persist($site);

        $manager->flush();
    }
}
