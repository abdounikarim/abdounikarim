<?php

namespace App\DataFixtures;

use App\Entity\Profile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProfileFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $profile = new Profile();
        $profile->setFirstname('Abdelkarim');
        $profile->setLastname('Abdouni');
        $profile->setImage('karim.png');
        $profile->setEmail('abdounikarim@gmail.com');
        $profile->setTitle('Développeur FullStack');
        $profile->setMobile('0645655016');
        $profile->setDescription('Intégration, Développement Frontend et Backend');
        $profile->setLinkedin('https://www.linkedin.com/in/abdelkarim-abdouni/');
        $profile->setGithub('https://github.com/abdounikarim');
        $profile->setFacebook('https://www.facebook.com/casrime');

        $manager->persist($profile);
        $manager->flush();
    }
}
