<?php

namespace App\DataFixtures;

use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ProjectFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $webagency = new Project();
        $webagency->setName('Webagency');
        $webagency->setImage('webagency.png');
        $webagency->setUrl('https://webagency.abdounikarim.com');
        $webagency->setSpecialties($this->getReference('integration'));
        $manager->persist($webagency);

        $strasbourg = new Project();
        $strasbourg->setName('Strasbourg');
        $strasbourg->setImage('strasbourg.jpg');
        $strasbourg->setUrl('https://strasbourg.abdounikarim.com');
        $strasbourg->setSpecialties($this->getReference('backend'));
        $manager->persist($strasbourg);

        $velib = new Project();
        $velib->setName('Velib');
        $velib->setImage('velib.jpg');
        $velib->setUrl('https://velib.abdounikarim.com');
        $velib->setSpecialties($this->getReference('frontend'));
        $manager->persist($velib);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            SpecialtyFixtures::class,
            SkillFixtures::class
        ];
    }

}
