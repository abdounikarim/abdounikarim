<?php

namespace App\DataFixtures;

use App\Entity\Info;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class InfoFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $info1= new Info();
        $info1->setTitle('Développeur');
        $info1->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac ipsum at quam lobortis hendrerit sit amet sit amet felis. Nullam rhoncus consectetur magna nec vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam, ante quis accumsan vehicula, lectus ligula vestibulum purus, in suscipit neque augue vel dolor. Proin lacinia luctus est, a fringilla mi dictum sit amet. Nam aliquet urna quis ornare pharetra. Nunc hendrerit convallis bibendum. Cras ultricies mattis vulputate. Suspendisse vestibulum nisi non leo convallis finibus. Aliquam accumsan viverra tincidunt. Donec pulvinar leo id luctus malesuada. Suspendisse viverra arcu vel mi rutrum, sed rutrum nunc tempus. Sed elit augue, rutrum vitae neque vitae, varius vestibulum dolor. Etiam sed arcu vitae leo tincidunt ornare luctus sit amet est.');
        $info1->setImage('developpeur.jpg');
        $info1->setIcon('<ion-icon name="ios-code" class="ios-lock"></ion-icon>');
        $manager->persist($info1);

        $info2= new Info();
        $info2->setTitle('Formateur');
        $info2->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac ipsum at quam lobortis hendrerit sit amet sit amet felis. Nullam rhoncus consectetur magna nec vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam, ante quis accumsan vehicula, lectus ligula vestibulum purus, in suscipit neque augue vel dolor. Proin lacinia luctus est, a fringilla mi dictum sit amet. Nam aliquet urna quis ornare pharetra. Nunc hendrerit convallis bibendum. Cras ultricies mattis vulputate. Suspendisse vestibulum nisi non leo convallis finibus. Aliquam accumsan viverra tincidunt. Donec pulvinar leo id luctus malesuada. Suspendisse viverra arcu vel mi rutrum, sed rutrum nunc tempus. Sed elit augue, rutrum vitae neque vitae, varius vestibulum dolor. Etiam sed arcu vitae leo tincidunt ornare luctus sit amet est.');
        $info2->setImage('formateur.jpg');
        $info2->setIcon('<ion-icon name="ios-help" class="ios-lock"></ion-icon>');
        $manager->persist($info2);

        $info3= new Info();
        $info3->setTitle('Auteur');
        $info3->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac ipsum at quam lobortis hendrerit sit amet sit amet felis. Nullam rhoncus consectetur magna nec vulputate. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam, ante quis accumsan vehicula, lectus ligula vestibulum purus, in suscipit neque augue vel dolor. Proin lacinia luctus est, a fringilla mi dictum sit amet. Nam aliquet urna quis ornare pharetra. Nunc hendrerit convallis bibendum. Cras ultricies mattis vulputate. Suspendisse vestibulum nisi non leo convallis finibus. Aliquam accumsan viverra tincidunt. Donec pulvinar leo id luctus malesuada. Suspendisse viverra arcu vel mi rutrum, sed rutrum nunc tempus. Sed elit augue, rutrum vitae neque vitae, varius vestibulum dolor. Etiam sed arcu vitae leo tincidunt ornare luctus sit amet est.');
        $info3->setImage('auteur.jpg');
        $info3->setIcon('<ion-icon name="ios-list" class="ios-lock"></ion-icon>');
        $manager->persist($info3);

        $manager->flush();
    }
}
