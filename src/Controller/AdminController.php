<?php

namespace App\Controller;

use App\Repository\ExperienceRepository;
use App\Repository\InfoRepository;
use App\Repository\LegalRepository;
use App\Repository\MailRepository;
use App\Repository\NetworkRepository;
use App\Repository\ProfileRepository;
use App\Repository\ProjectRepository;
use App\Repository\ServiceRepository;
use App\Repository\SkillRepository;
use App\Repository\SpecialtyRepository;
use App\Repository\VideoRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="admin_home")
     */
    public function index(ProfileRepository $profileRepository, InfoRepository $infoRepository, SkillRepository $skillRepository, ExperienceRepository $experienceRepository, VideoRepository $videoRepository, ServiceRepository $serviceRepository, SpecialtyRepository $specialtyRepository, ProjectRepository $projectRepository, NetworkRepository $networkRepository, MailRepository $mailRepository, LegalRepository $legalRepository)
    {
        return $this->render('admin/index.html.twig', [
            'profiles' => $profileRepository->findAll(),
            'infos' => $infoRepository->findAll(),
            'skills' => $skillRepository->findAll(),
            'experiences' => $experienceRepository->findAll(),
            'videos' => $videoRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'specialties' => $specialtyRepository->findAll(),
            'projects' => $projectRepository->findAll(),
            'networks' => $networkRepository->findAll(),
            'mails' => $mailRepository->findAll(),
            'legals' => $legalRepository->findAll()
        ]);
    }
}
