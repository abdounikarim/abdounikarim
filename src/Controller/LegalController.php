<?php

namespace App\Controller;

use App\Entity\Legal;
use App\Form\LegalType;
use App\Repository\LegalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/legal")
 */
class LegalController extends Controller
{
    /**
     * @Route("/", name="legal_index", methods="GET")
     */
    public function index(LegalRepository $legalRepository): Response
    {
        return $this->render('legal/index.html.twig', ['legals' => $legalRepository->findAll()]);
    }

    /**
     * @Route("/new", name="legal_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $legal = new Legal();
        $form = $this->createForm(LegalType::class, $legal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($legal);
            $em->flush();

            return $this->redirectToRoute('admin_home');
        }

        return $this->render('legal/new.html.twig', [
            'legal' => $legal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="legal_show", methods="GET")
     */
    public function show(Legal $legal): Response
    {
        return $this->render('legal/show.html.twig', ['legal' => $legal]);
    }

    /**
     * @Route("/{id}/edit", name="legal_edit", methods="GET|POST")
     */
    public function edit(Request $request, Legal $legal): Response
    {
        $form = $this->createForm(LegalType::class, $legal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('legal_edit', ['id' => $legal->getId()]);
        }

        return $this->render('legal/edit.html.twig', [
            'legal' => $legal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="legal_delete", methods="DELETE")
     */
    public function delete(Request $request, Legal $legal): Response
    {
        if ($this->isCsrfTokenValid('delete'.$legal->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($legal);
            $em->flush();
        }

        return $this->redirectToRoute('admin_home');
    }
}
