<?php

namespace App\Controller;

use App\Entity\Network;
use App\Form\NetworkType;
use App\Repository\NetworkRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/network")
 */
class NetworkController extends Controller
{
    /**
     * @Route("/", name="network_index", methods="GET")
     */
    public function index(NetworkRepository $networkRepository): Response
    {
        return $this->render('network/index.html.twig', ['networks' => $networkRepository->findAll()]);
    }

    /**
     * @Route("/new", name="network_new", methods="GET|POST")
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $network = new Network();
        $form = $this->createForm(NetworkType::class, $network);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['image']->getData();
            $fileName = $fileUploader->upload($file);
            $network->setImage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($network);
            $em->flush();

            return $this->redirectToRoute('admin_home');
        }

        return $this->render('network/new.html.twig', [
            'network' => $network,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="network_show", methods="GET")
     */
    public function show(Network $network): Response
    {
        return $this->render('network/show.html.twig', ['network' => $network]);
    }

    /**
     * @Route("/{id}/edit", name="network_edit", methods="GET|POST")
     */
    public function edit(Request $request, Network $network, FileUploader $fileUploader): Response
    {
        $form = $this->createForm(NetworkType::class, $network);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['image']->getData();
            $fileName = $fileUploader->upload($file);
            $network->setImage($fileName);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('network_edit', ['id' => $network->getId()]);
        }

        return $this->render('network/edit.html.twig', [
            'network' => $network,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="network_delete", methods="DELETE")
     */
    public function delete(Request $request, Network $network): Response
    {
        if ($this->isCsrfTokenValid('delete'.$network->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($network);
            $em->flush();
        }

        return $this->redirectToRoute('admin_home');
    }
}
