<?php

namespace App\Controller;

use App\Entity\Mail;
use App\Entity\Network;
use App\Form\MailType;
use App\Form\NetworkType;
use App\Repository\ExperienceRepository;
use App\Repository\InfoRepository;
use App\Repository\LegalRepository;
use App\Repository\NetworkRepository;
use App\Repository\ProfileRepository;
use App\Repository\ProjectRepository;
use App\Repository\ServiceRepository;
use App\Repository\SkillRepository;
use App\Repository\SpecialtyRepository;
use App\Repository\VideoRepository;
use App\Service\FileUploader;
use App\Service\SendMail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    /**
     * @Route("/", name="accueil")
     */
    public function index(ProfileRepository $profileRepository, ProjectRepository $projectRepository)
    {
        return $this->render('home/index.html.twig', [
            'profiles' => $profileRepository->findAll(),
            'projects' => $projectRepository->getThreeLastProjects()
        ]);
    }

    /**
     * @Route("/info", name="info")
     */
    public function info(InfoRepository $infoRepository, SkillRepository $skillRepository, ExperienceRepository $experienceRepository, VideoRepository $videoRepository)
    {
        return $this->render('home/info.html.twig', [
            'infos' => $infoRepository->findAll(),
            'skills' => $skillRepository->findAll(),
            'experiences' => $experienceRepository->findAll(),
            'videos' => $videoRepository->findAll()
        ]);
    }

    /**
     * @Route("/service", name="service")
     */
    public function service(ServiceRepository $serviceRepository)
    {
        return $this->render('home/service.html.twig', [
            'services' => $serviceRepository->findAll()
        ]);
    }

    /**
     * @Route("/portfolio", name="portfolio")
     */
    public function portfolio(SpecialtyRepository $specialtyRepository, SkillRepository $skillRepository, ProjectRepository $projectRepository)
    {
        return $this->render('home/portfolio.html.twig', [
            'specialties' => $specialtyRepository->findAll(),
            'skills' => $skillRepository->findAll(),
            'projects' => $projectRepository->findAll()
        ]);
    }

    /**
     * @Route("/equipe", name="equipe")
     */
    public function equipe(Request $request, NetworkRepository $networkRepository, FileUploader $fileUploader)
    {
        $network = new Network();
        $form = $this->createForm(NetworkType::class, $network);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $file = $form['image']->getData();
            $filename = $fileUploader->upload($file);
            $network->setImage($filename);
            $network->setTeam(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($network);
            $em->flush();
            $this->addFlash('add_evaluation', 'Votre évaluation a bien été ajoutée');
            return $this->redirectToRoute('equipe');
        }
        return $this->render('home/equipe.html.twig', [
            'members' => $networkRepository->getMembers(),
            'form' => $form->createView(),
            'others' => $networkRepository->getOthers()
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, ProfileRepository $profileRepository, SendMail $sendMail)
    {
        $mail = new Mail();
        $form = $this->createForm(MailType::class, $mail);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $sendMail->send($form->getData());
            $sendMail->save($form->getData());
            $this->addFlash('mail', 'Votre message a bien été envoyé, merci');
            return $this->redirectToRoute('accueil');
        }
        return $this->render('home/contact.html.twig', [
            'form' => $form->createView(),
            'profile' => $profileRepository->find(1)
        ]);
    }

    /**
     * @Route("/legal", name="legal")
     */
    public function legal(LegalRepository $legalRepository)
    {
        return $this->render('home/legal.html.twig', [
            'legals' => $legalRepository->findAll()
        ]);
    }
}
