<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Tech;
use App\Form\ProjectType;
use App\Form\TechType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function index()
    {

        return $this->render('default/index.html.twig');
    }

    public function portfolio()
    {
        $em = $this->getDoctrine()->getManager();
        $allTechs = $em->getRepository('App:Tech')->findAll();

        return $this->render('default/portfolio.html.twig', [
            'allTechs' => $allTechs,
        ]);
    }

    public function techs()
    {
        $em = $this->getDoctrine()->getManager();
        $allProjects = $em->getRepository('App:Project')->findAllActiveProjects();
        $data = [];
        foreach ($allProjects as $project) {
            $techs = [];
            foreach ($project->getTechs() as $tech){
                array_push($techs, $tech->getName());
            }
            $data[] = [
                'id' => $project->getId(),
                'name' => $project->getName(),
                'image' => $project->getImage(),
                'description' => $project->getDescription(),
                'techs' => $techs
            ];
        }
        $response = new JsonResponse();
        return $response->setData([
            'data' => $data,
        ]);
    }

    public function tech($id)
    {
        $em = $this->getDoctrine()->getManager();
        $projects = $em->getRepository('App:Project')->getProjectsByTechId($id);
        $data = [];
        foreach ($projects as $project)
        {
            $data[] = [
                'id' => $project->getId(),
                'name' => $project->getName(),
                'image' => $project->getImage(),
                'description' => $project->getDescription(),
            ];
        }
        $response = new JsonResponse();
        return $response->setData([
            'data' => $data,
        ]);
    }

    public function addTech(Request $request)
    {
        $tech = new Tech();
        $form = $this->createForm(TechType::class, $tech);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();
        }
        return $this->render('form/tech.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function addProject(Request $request)
    {
        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();
        }
        return $this->render('form/project.html.twig', [
            'form' => $form->createView()
        ]);
    }
}