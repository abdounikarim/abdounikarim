<?php

namespace App\Repository;

use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function findAllActiveProjects()
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT p,t FROM App:Project p
                  JOIN p.techs t
                  WHERE p.active = 1'
        );
        $result = $query->getResult();
        return $result;
    }

    public function getProjectsByTechId($tech_id)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT p, t FROM App:Project p
                  JOIN p.techs t
                  WHERE t.id = :id
                  AND p.active = 1'
        )->setParameter('id', $tech_id);
        $result = $query->getResult();
        return $result;
    }
    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('p')
            ->where('p.something = :value')->setParameter('value', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
