<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180722170908 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE project_specialty');
        $this->addSql('ALTER TABLE project ADD specialties_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEE3AC3692 FOREIGN KEY (specialties_id) REFERENCES specialty (id)');
        $this->addSql('CREATE INDEX IDX_2FB3D0EEE3AC3692 ON project (specialties_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE project_specialty (project_id INT NOT NULL, specialty_id INT NOT NULL, INDEX IDX_F15AE33B166D1F9C (project_id), INDEX IDX_F15AE33B9A353316 (specialty_id), PRIMARY KEY(project_id, specialty_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project_specialty ADD CONSTRAINT FK_F15AE33B166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_specialty ADD CONSTRAINT FK_F15AE33B9A353316 FOREIGN KEY (specialty_id) REFERENCES specialty (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEE3AC3692');
        $this->addSql('DROP INDEX IDX_2FB3D0EEE3AC3692 ON project');
        $this->addSql('ALTER TABLE project DROP specialties_id');
    }
}
