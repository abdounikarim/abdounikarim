<?php

namespace App\Form;

use App\Entity\Profile;
use App\Entity\Social;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'Votre prénom'
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Votre nom'
            ])
            ->add('image', FileType::class, [
                'label' => 'Votre image',
                'data_class' => null
            ])
            ->add('title', TextType::class, [
                'label' => 'Votre poste'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description'
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email'
            ])
            ->add('mobile', TextType::class, [
                'label' => 'Téléphone'
            ])
            ->add('linkedin', UrlType::class, [
                'label' => 'Linkedin'
            ])
            ->add('github', UrlType::class, [
                'label' => 'GitHub'
            ])
            ->add('facebook', UrlType::class, [
                'label' => 'Facebook'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Profile::class,
        ]);
    }
}
