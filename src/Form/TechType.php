<?php

namespace App\Form;

use App\Entity\Tech;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class TechType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom de la tech',
                'constraints' => [
                    new NotBlank(['message' => 'Merci de renseigner un nom de techno'])
                ]

            ])
            ->add('image', TextType::class, [
                'label' => 'Image associée',
                'constraints' => [
                    new NotBlank(['message' => 'Merci de renseigner un lien d\'image'])
                ]

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tech::class,
        ]);
    }
}
