<?php

namespace App\Tests\Entity;

use App\Entity\Mail;
use PHPUnit\Framework\TestCase;

class MailTest extends TestCase
{
    private $mail;

    public function setUp()
    {
        $this->mail = new Mail();
    }

    public function testMailIsInstanceOfClassMail()
    {
        $this->assertInstanceOf(Mail::class, $this->mail);
    }

    public function testIdIsNull()
    {
        $this->assertNull($this->mail->getId());
    }

    public function testNameIsValid()
    {
        $this->mail->setName('name');
        $this->assertEquals('name', $this->mail->getName());
    }

    public function testEmailIsValid()
    {
        $this->mail->setEmail('name');
        $this->assertEquals('name', $this->mail->getEmail());
    }

    public function testSubjectIsValid()
    {
        $this->mail->setSubject('blabla');
        $this->assertEquals('blabla', $this->mail->getSubject());
    }

    public function testMessageIsValid()
    {
        $this->mail->setMessage('plop');
        $this->assertEquals('plop', $this->mail->getMessage());
    }
}