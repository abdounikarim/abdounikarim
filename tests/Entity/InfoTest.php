<?php

namespace App\Tests\Entity;

use App\Entity\Info;
use PHPUnit\Framework\TestCase;

class InfoTest extends TestCase
{
    private $info;

    public function setUp()
    {
        $this->info = new Info();
    }

    public function testIdIsNull()
    {
        $this->assertNull($this->info->getId());
    }

    public function testTitleIsValid()
    {
        $this->info->setTitle('title');
        $this->assertEquals('title', $this->info->getTitle());
    }

    public function testDescriptionIsValid()
    {
        $this->info->setDescription('lorem ipsum');
        $this->assertEquals('lorem ipsum', $this->info->getDescription());
    }

    public function testImageIsValid()
    {
        $this->info->setImage('monimage.png');
        $this->assertEquals('monimage.png', $this->info->getImage());
    }

    public function testIconIsValid()
    {
        $this->info->setIcon('monicone.png');
        $this->assertEquals('monicone.png', $this->info->getIcon());
    }
}
