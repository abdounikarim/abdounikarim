<?php

namespace App\Tests\Entity;

use App\Entity\Profile;
use PHPUnit\Framework\TestCase;

class ProfileTest extends TestCase
{
    private $profile;

    public function setUp()
    {
        $this->profile = new Profile();
    }

    public function testIdIsNull()
    {
        $this->assertNull($this->profile->getId());
    }

    public function testFirstnameIsValid()
    {
        $this->profile->setFirstname('Toto');
        $this->assertEquals('Toto', $this->profile->getFirstname());
    }

    public function testLastnameIsValid()
    {
        $this->profile->setLastname('Tata');
        $this->assertEquals('Tata', $this->profile->getLastname());
    }

    public function testEmailIsValid()
    {
        $this->profile->setEmail('toto@tata.com');
        $this->assertEquals('toto@tata.com', $this->profile->getEmail());
    }

    public function testImageIsValid()
    {
        $this->profile->setImage('toto.png');
        $this->assertEquals('toto.png', $this->profile->getImage());
    }

    public function testTitleIsValid()
    {
        $this->profile->setTitle('title');
        $this->assertEquals('title', $this->profile->getTitle());
    }

    public function testDescriptionIsValid()
    {
        $this->profile->setDescription('Lorem ipsum');
        $this->assertEquals('Lorem ipsum', $this->profile->getDescription());
    }

    public function testMobileIsValid()
    {
        $this->profile->setMobile('0612345678');
        $this->assertEquals('0612345678', $this->profile->getMobile());
    }

    public function testLinkedinIsValid()
    {
        $this->profile->setLinkedin('https://linkedin.com');
        $this->assertEquals('https://linkedin.com', $this->profile->getLinkedin());
    }

    public function testGithubIsValid()
    {
        $this->profile->setGithub('https://github.com');
        $this->assertEquals('https://github.com', $this->profile->getGithub());
    }

    public function testFacebookIsValid()
    {
        $this->profile->setFacebook('https://facebook.com');
        $this->assertEquals('https://facebook.com', $this->profile->getFacebook());
    }
}