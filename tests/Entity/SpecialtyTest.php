<?php

namespace App\Tests\Entity;

use App\Entity\Specialty;
use PHPUnit\Framework\TestCase;

class SpecialtyTest extends TestCase
{
    private $specialty;

    public function setUp()
    {
        $this->specialty = new Specialty();
    }

    public function testIdIsNull()
    {
        $this->assertNull($this->specialty->getId());
    }

    public function testNameIsValid()
    {
        $this->specialty->setName('specialty');
        $this->assertEquals('specialty', $this->specialty->getName());
    }
}