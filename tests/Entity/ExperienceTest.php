<?php

namespace App\Tests\Entity;

use App\Entity\Experience;
use PHPUnit\Framework\TestCase;

class ExperienceTest extends TestCase
{
    private $experience;

    public function setUp()
    {
        $this->experience = new Experience();
    }

    public function testIdIsNull()
    {
        $this->assertNull($this->experience->getId());
    }

    public function testTitleIsValid()
    {
        $this->experience->setTitle('title');
        $this->assertEquals('title', $this->experience->getTitle());
    }

    public function testPeriodIsValid()
    {
        $this->experience->setPeriod('january');
        $this->assertEquals('january', $this->experience->getPeriod());
    }

    public function testImageIsValid()
    {
        $this->experience->setImage('monimage.png');
        $this->assertEquals('monimage.png', $this->experience->getImage());
    }

    public function testDescriptionIsValid()
    {
        $this->experience->setDescription('lorem ipsum');
        $this->assertEquals('lorem ipsum', $this->experience->getDescription());
    }
}
