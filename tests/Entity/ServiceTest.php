<?php

namespace App\Tests\Entity;

use App\Entity\Service;
use PHPUnit\Framework\TestCase;

class ServiceTest extends TestCase
{
    private $service;

    public function setUp()
    {
        $this->service = new Service();
    }

    public function testIdIsNull()
    {
        $this->assertNull($this->service->getId());
    }

    public function testNameIsValid()
    {
        $this->service->setName('service');
        $this->assertEquals('service', $this->service->getName());
    }

    public function testDescriptionIsValid()
    {
        $this->service->setDescription('service description');
        $this->assertEquals('service description', $this->service->getDescription());
    }

    public function testImageIsValid()
    {
        $this->service->setImage('image.png');
        $this->assertEquals('image.png', $this->service->getImage());
    }

    public function testIconIsValid()
    {
        $this->service->setIcon('icon.png');
        $this->assertEquals('icon.png', $this->service->getIcon());
    }
}