<?php

namespace App\Tests\Entity;

use App\Entity\Project;
use App\Entity\Skill;
use App\Entity\Specialty;
use PHPUnit\Framework\TestCase;

class ProjectTest extends TestCase
{
    private $project;

    public function setUp()
    {
        $this->project = new Project();
    }

    public function testIdIsNull()
    {
        $this->assertNull($this->project->getId());
    }

    public function testNameIsValid()
    {
        $this->project->setName('project');
        $this->assertEquals('project', $this->project->getName());
    }

    public function testUrlIsValid()
    {
        $this->project->setUrl('https://url.com');
        $this->assertEquals('https://url.com', $this->project->getUrl());
    }

    public function testImageIsValid()
    {
        $this->project->setImage('image.png');
        $this->assertEquals('image.png', $this->project->getImage());
    }

    public function testAddSkill()
    {
        $this->project->addSkill(new Skill());
        $this->assertCount(1, $this->project->getSkills());
    }

    public function testSpecialtiesAreValid()
    {
        $specialty = new Specialty();
        $this->project->setSpecialties($specialty);
        $this->assertEquals($specialty, $this->project->getSpecialties());
        $this->assertInstanceOf(Specialty::class, $specialty);
    }
}