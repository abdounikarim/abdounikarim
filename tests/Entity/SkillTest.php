<?php

namespace App\Tests\Entity;

use App\Entity\Skill;
use PHPUnit\Framework\TestCase;

class SkillTest extends TestCase
{
    private $skill;

    public function setUp()
    {
        $this->skill = new Skill();
    }

    public function testIdIsNull()
    {
        $this->assertNull($this->skill->getId());
    }

    public function testNameIsValid()
    {
        $this->skill->setName('skill');
        $this->assertEquals('skill', $this->skill->getName());
    }

    public function testImageIsValid()
    {
        $this->skill->setImage('image.png');
        $this->assertEquals('image.png', $this->skill->getImage());
    }
}