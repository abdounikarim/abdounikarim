<?php

namespace App\Tests\Entity;

use App\Entity\Video;
use PHPUnit\Framework\TestCase;

class VideoTest extends TestCase
{
    private $video;

    public function setUp()
    {
        $this->video = new Video();
    }

    public function testIdIsNull()
    {
        $this->assertNull($this->video->getId());
    }

    public function testNameIsValid()
    {
        $this->video->setName('video');
        $this->assertEquals('video', $this->video->getName());
    }

    public function testUrlIsValid()
    {
        $this->video->setUrl('https://youtube.com');
        $this->assertEquals('https://youtube.com', $this->video->getUrl());
    }
}