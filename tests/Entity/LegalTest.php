<?php

namespace App\Tests\Entity;

use App\Entity\Legal;
use PHPUnit\Framework\TestCase;

class LegalTest extends TestCase
{
    private $legal;

    public function setUp()
    {
        $this->legal = new Legal();
    }

    public function testIdIsNull()
    {
        $this->assertNull($this->legal->getId());
    }

    public function testTitleIsValid()
    {
        $this->legal->setTitle('title');
        $this->assertEquals('title', $this->legal->getTitle());
    }

    public function testContentIsValid()
    {
        $this->legal->setContent('lorem ipsum');
        $this->assertEquals('lorem ipsum', $this->legal->getContent());
    }
}
