<?php

namespace App\Tests\Entity;

use App\Entity\Network;
use PHPUnit\Framework\TestCase;

class NetworkTest extends TestCase
{
    private $network;

    public function setUp()
    {
        $this->network = new Network();
    }

    public function testNetworkObjectIsFromNetworkClass()
    {
        $this->assertInstanceOf(Network::class, $this->network);
    }

    public function testIdIsNull()
    {
        $this->assertNull($this->network->getId());
    }

    public function testFirstnameIsValid()
    {
        $this->network->setFirstname('Karim');
        $this->assertEquals('Karim', $this->network->getFirstname());
    }

    public function testFirstnameIsNotValid()
    {
        $this->network->setFirstname('Karim');
        $this->assertNotEquals('Julien', $this->network->getFirstname());
    }

    public function testLastnameIsValid()
    {
        $this->network->setLastname('toto');
        $this->assertSame('toto', $this->network->getLastname());
    }

    public function testLastnameIsNotValid()
    {
        $this->network->setLastname('toto');
        $this->assertNotEquals('tata', $this->network->getLastname());
    }

    public function testImageIsValid()
    {
        $this->network->setImage('monimage.png');
        $this->assertEquals('monimage.png', $this->network->getImage());
    }

    public function testTitleIsValid()
    {
        $this->network->setTitle('Développeur');
        $this->assertEquals('Développeur', $this->network->getTitle());
    }

    public function testDescriptionIsValid()
    {
        $this->network->setDescription('Lorem ipsum');
        $this->assertEquals('Lorem ipsum', $this->network->getDescription());
    }

    public function testTeamIsTrue()
    {
        $this->network->setTeam(1);
        $this->assertTrue($this->network->getTeam());
        $this->assertEquals(1, $this->network->getTeam());
    }

    public function testEmailIsValid()
    {
        $this->network->setEmail('a@a.com');
        $this->assertEquals('a@a.com', $this->network->getEmail());
    }

    public function testStarIsLowerThanSix()
    {
        $this->network->setStar(5);
        $this->assertEquals(5, $this->network->getStar());
    }

    public function testSiteIsValid()
    {
        $this->network->setSite('https://monsite.com');
        $this->assertEquals('https://monsite.com', $this->network->getSite());
    }

    public function testLinkedinIsValid()
    {
        $this->network->setLinkedin('https://linkedin.com');
        $this->assertEquals('https://linkedin.com', $this->network->getLinkedin());
    }

    public function testGithubIsValid()
    {
        $this->network->setGithub('https://github.com');
        $this->assertEquals('https://github.com', $this->network->getGithub());
    }

    public function testFacebookIsValid()
    {
        $this->network->setFacebook('https://facebook.com');
        $this->assertEquals('https://facebook.com', $this->network->getFacebook());
    }

    public function testStarIsGreaterThanOrEqualZero()
    {
        $this->network->setStar(0);
        $this->assertGreaterThanOrEqual(0, $this->network->getStar());
    }
}
